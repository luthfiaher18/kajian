package com.mtxl.listkajian.service.repository;

import com.mtxl.listkajian.service.model.AddKajianRq;
import com.mtxl.listkajian.service.model.KajianTrx;
import com.mtxl.listkajian.service.model.ListKajianRs;

import java.util.List;

public interface SourceListKajian {

    public List<ListKajianRs> getDataWeek();

    public List<ListKajianRs> getDataLive();

    public List<ListKajianRs> getDataArchive();

    public void insertKajianMaster(KajianTrx kajianTrx);

}
