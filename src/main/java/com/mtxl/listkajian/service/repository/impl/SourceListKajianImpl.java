package com.mtxl.listkajian.service.repository.impl;

import com.mtxl.listkajian.service.model.KajianTrx;
import com.mtxl.listkajian.service.model.ListKajianRs;
import com.mtxl.listkajian.service.model.rowmapper.ListKajianRsRowMapper;
import com.mtxl.listkajian.service.repository.SourceListKajian;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;

@Transactional
@Repository
public class SourceListKajianImpl implements SourceListKajian {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<ListKajianRs> getDataWeek() {
        String query = "SELECT * FROM mtxl_kajian_master WHERE YEARWEEK(`KAJIAN_DATE`, 1) = YEARWEEK(CURDATE(), 1)";
        return jdbcTemplate.query(query,new ListKajianRsRowMapper());
    }

    @Override
    public List<ListKajianRs> getDataLive() {
        String query = "SELECT * FROM mtxl_kajian_master WHERE `KAJIAN_DATE`= CURDATE()";
        return jdbcTemplate.query(query,new ListKajianRsRowMapper());
    }

    @Override
    public List<ListKajianRs> getDataArchive() {
        String query = "SELECT * FROM mtxl_kajian_master WHERE `KAJIAN_DATE`< CURDATE()";
        return jdbcTemplate.query(query,new ListKajianRsRowMapper());
    }

    @Override
    public void insertKajianMaster(KajianTrx kajianTrx) {
        String query = "INSERT INTO mtxl_kajian_master(`KAJIAN_NAME`,`SPEAKER`,`SPEAKER_PICT_URL`,`LOCATION`,`TIME`,`KAJIAN_DATE`,`LIVE_URL`,`QNA_URL`,`DAY`)\n" +
                "VALUES (?,?,?,?,?,?,?,?,?)";
        jdbcTemplate.update(query, new Object[] {kajianTrx.getKajianName(),kajianTrx.getSpeakerName(),kajianTrx.getSpeakerPictUrl(),kajianTrx.getLocation(),kajianTrx.getTime(),
        kajianTrx.getKajianDate(),kajianTrx.getLiveUrl(),kajianTrx.getQnaUrl(),kajianTrx.getDay()});

    }
}
