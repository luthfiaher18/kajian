package com.mtxl.listkajian.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalendarUtil {

    private static final Logger logger = LoggerFactory.getLogger(CalendarUtil.class);

    public String getDayIdn(String dayEng){
        String dayIdn = "";
        switch (dayEng.toUpperCase()) {
            case "MONDAY":
                dayIdn = "Senin";
                break;
            case "TUESDAY":
                dayIdn = "Selasa";
                break;
            case "WEDNESDAY":
                dayIdn = "Rabu";
                break;
            case "THURSDAY":
                dayIdn = "Kamis";
                break;
            case "FRIDAY":
                dayIdn = "Jumat";
                break;
            case "SATURDAY":
                dayIdn = "Sabtu";
                break;
            case "SUNDAY":
                dayIdn = "Minggu";
                break;
            default:
                dayIdn = dayEng;
        }
        return dayIdn;
    }

}
