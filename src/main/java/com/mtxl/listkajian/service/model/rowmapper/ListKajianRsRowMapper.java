package com.mtxl.listkajian.service.model.rowmapper;

import com.mtxl.listkajian.service.model.ListKajianRs;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ListKajianRsRowMapper implements RowMapper<ListKajianRs> {

    @Override
    public ListKajianRs mapRow(ResultSet rs, int i) throws SQLException {
        ListKajianRs listKajianRs = new ListKajianRs();
        listKajianRs.setKajianName(rs.getString("KAJIAN_NAME"));
        listKajianRs.setSpeakerName(rs.getString("SPEAKER"));
        listKajianRs.setSpeakerPictUrl(rs.getString("SPEAKER_PICT_URL"));
        listKajianRs.setLocation(rs.getString("LOCATION"));
        listKajianRs.setTime(rs.getString("TIME"));
        listKajianRs.setKajianDate(rs.getDate("KAJIAN_DATE"));
        listKajianRs.setLiveUrl(rs.getString("LIVE_URL"));
        listKajianRs.setQnaUrl(rs.getString("QNA_URL"));
        listKajianRs.setVideoUrl(rs.getString("VIDEO_URL"));
        listKajianRs.setDay(rs.getString("DAY"));
        return listKajianRs;
    }
}
