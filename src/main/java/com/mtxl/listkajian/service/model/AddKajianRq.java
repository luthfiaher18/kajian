	package com.mtxl.listkajian.service.model;

	import com.fasterxml.jackson.annotation.*;

	import java.util.HashMap;
	import java.util.Map;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({
			"kajianName",
			"speakerName",
			"speakerPictUrl",
			"location",
			"time",
			"kajianDate",
			"liveUrl",
			"qnaUrl",
			"day"
	})
	public class AddKajianRq {

		@JsonProperty("kajianName")
		private String kajianName;

		@JsonProperty("speakerName")
		private String speakerName;

		@JsonProperty("speakerPictUrl")
		private String speakerPictUrl;

		@JsonProperty("location")
		private String location;

		@JsonProperty("time")
		private String time;

		@JsonProperty("kajianDate")
		private String kajianDate;

		@JsonProperty("liveUrl")
		private String liveUrl;

		@JsonProperty("qnaUrl")
		private String qnaUrl;

		@JsonProperty("day")
		private String day;

		@JsonIgnore
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();

		//============================================================//

		@JsonProperty("kajianName")
		public String getKajianName() {
			return kajianName;
		}

		@JsonProperty("kajianName")
		public void setKajianName(String kajianName) {
			this.kajianName = kajianName;
		}

		public AddKajianRq withKajianName(String kajianName) {
			this.kajianName = kajianName;
			return this;
		}

		@JsonProperty("speakerName")
		public String getSpeakerName() {
			return speakerName;
		}

		@JsonProperty("speakerName")
		public void setSpeakerName(String speakerName) {
			this.speakerName = speakerName;
		}

		public AddKajianRq withSpeakerName(String speakerName) {
			this.speakerName = speakerName;
			return this;
		}

		@JsonProperty("speakerPictUrl")
		public String getSpeakerPictUrl() {
			return speakerPictUrl;
		}

		@JsonProperty("speakerPictUrl")
		public void setSpeakerPictUrl(String speakerPictUrl) {
			this.speakerPictUrl = speakerPictUrl;
		}

		public AddKajianRq withSpeakerPictUrl(String speakerPictUrl) {
			this.speakerPictUrl = speakerPictUrl;
			return this;
		}

		@JsonProperty("location")
		public String getLocation() {
			return location;
		}

		@JsonProperty("location")
		public void setLocation(String location) {
			this.location = location;
		}

		public AddKajianRq withLocation(String location) {
			this.location = location;
			return this;
		}

		@JsonProperty("time")
		public String getTime() {
			return time;
		}

		@JsonProperty("time")
		public void setTime(String time) {
			this.time = time;
		}

		public AddKajianRq withTime(String time) {
			this.time = time;
			return this;
		}

		@JsonProperty("kajianDate")
		public String getKajianDate() {
			return kajianDate;
		}

		@JsonProperty("kajianDate")
		public void setKajianDate(String kajianDate) {
			this.kajianDate = kajianDate;
		}

		public AddKajianRq withKajianDate(String kajianDate) {
			this.kajianDate = kajianDate;
			return this;
		}

		@JsonProperty("liveUrl")
		public String getLiveUrl() {
			return liveUrl;
		}

		@JsonProperty("liveUrl")
		public void setLiveUrl(String liveUrl) {
			this.liveUrl = liveUrl;
		}

		public AddKajianRq withLiveUrl(String liveUrl) {
			this.liveUrl = liveUrl;
			return this;
		}

		@JsonProperty("qnaUrl")
		public String getQnaUrl() {
			return qnaUrl;
		}

		@JsonProperty("qnaUrl")
		public void setQnaUrl(String qnaUrl) {
			this.qnaUrl = qnaUrl;
		}

		public AddKajianRq withQnaUrl(String qnaUrl) {
			this.qnaUrl = qnaUrl;
			return this;
		}

		@JsonProperty("day")
		public String getDay() {
			return day;
		}

		@JsonProperty("day")
		public void setDay(String day) {
			this.day = day;
		}

		public AddKajianRq withDay(String day) {
			this.day = day;
			return this;
		}

		@JsonAnyGetter
		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		@JsonAnySetter
		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

		public AddKajianRq withAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
			return this;
		}
	}