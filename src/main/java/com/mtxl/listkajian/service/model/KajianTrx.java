package com.mtxl.listkajian.service.model;

public class KajianTrx {

    private String kajianName;
    private String speakerName;
    private String speakerPictUrl;
    private String location;
    private String time;
    private String kajianDate;
    private String liveUrl;
    private String qnaUrl;
    private String day;

    public KajianTrx(String kajianName, String speakerName, String speakerPictUrl, String location, String time, String kajianDate, String liveUrl, String qnaUrl, String day) {
        super();
        this.kajianName = kajianName;
        this.speakerName = speakerName;
        this.speakerPictUrl = speakerPictUrl;
        this.location = location;
        this.time = time;
        this.kajianDate = kajianDate;
        this.liveUrl = liveUrl;
        this.qnaUrl = qnaUrl;
        this.day = day;
    }

    public KajianTrx() {
        super();
    }

    public String getKajianName() {
        return kajianName;
    }

    public void setKajianName(String kajianName) {
        this.kajianName = kajianName;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public String getSpeakerPictUrl() {
        return speakerPictUrl;
    }

    public void setSpeakerPictUrl(String speakerPictUrl) {
        this.speakerPictUrl = speakerPictUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getKajianDate() {
        return kajianDate;
    }

    public void setKajianDate(String kajianDate) {
        this.kajianDate = kajianDate;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getQnaUrl() {
        return qnaUrl;
    }

    public void setQnaUrl(String qnaUrl) {
        this.qnaUrl = qnaUrl;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
