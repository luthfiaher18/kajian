package com.mtxl.listkajian.service.model;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ListKajianRs {

    private String kajianName;
    private String speakerName;
    private String speakerPictUrl;
    private String location;
    private String time;
    private Date kajianDate;
    private String liveUrl;
    private String qnaUrl;
    private String videoUrl;
    private String day;

    public ListKajianRs(String kajianName, String speakerName, String speakerPictUrl, String location, String time, Date kajianDate, String liveUrl, String qnaUrl, String videoUrl, String day) {
        super();
        this.kajianName = kajianName;
        this.speakerName = speakerName;
        this.speakerPictUrl = speakerPictUrl;
        this.location = location;
        this.time = time;
        this.kajianDate = kajianDate;
        this.liveUrl = liveUrl;
        this.qnaUrl = qnaUrl;
        this.videoUrl = videoUrl;
        this.day = day;
    }

    public ListKajianRs() {
        super();
    }

    public String getKajianName() {
        return kajianName;
    }

    public void setKajianName(String kajianName) {
        this.kajianName = kajianName;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public String getSpeakerPictUrl() {
        return speakerPictUrl;
    }

    public void setSpeakerPictUrl(String speakerPictUrl) {
        this.speakerPictUrl = speakerPictUrl;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getKajianDate() {
        return kajianDate;
    }

    public void setKajianDate(Date kajianDate) {
        this.kajianDate = kajianDate;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getQnaUrl() {
        return qnaUrl;
    }

    public void setQnaUrl(String qnaUrl) {
        this.qnaUrl = qnaUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "ListKajianRs{" +
                "kajianName='" + kajianName + '\'' +
                ", spekaerName='" + speakerName + '\'' +
                ", speakerPictUrl='" + speakerPictUrl + '\'' +
                ", location='" + location + '\'' +
                ", time='" + time + '\'' +
                ", kajianDate=" + kajianDate +
                ", liveUrl='" + liveUrl + '\'' +
                ", qnaUrl='" + qnaUrl + '\'' +
                ", videoUrl='" + videoUrl + '\'' +
                ", day='" + day + '\'' +
                '}';
    }
}
