package com.mtxl.listkajian.service.controller;

import com.mtxl.listkajian.service.model.AddKajianRq;
import com.mtxl.listkajian.service.model.GeneralResponse;
import com.mtxl.listkajian.service.model.KajianTrx;
import com.mtxl.listkajian.service.model.ListKajianRs;
import com.mtxl.listkajian.service.repository.impl.SourceListKajianImpl;
import com.mtxl.listkajian.service.util.CalendarUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api")
@Validated
public class ListKajian {

    private static final Logger logger = LoggerFactory.getLogger(ListKajian.class);

    @Autowired
    SourceListKajianImpl sourceListKajian;

    @Autowired
    ListKajianRs listKajianRs;


    @RequestMapping(value = "v1/kajian/add", method = RequestMethod.POST)
    public ResponseEntity<?> manualWrite(@RequestBody AddKajianRq addKajianRq,
                                         @RequestHeader(value = "requestdate", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") String requestdate,
                                         @RequestHeader(value = "channel", required = false) String channel) throws Exception{

        logger.info("============ REQUEST ADD LIST KAJIAN WITH CHANNEL {} AND REQUESTDATE {} ============",channel,requestdate);
        logger.info("WITH BODY {}", addKajianRq);
        GeneralResponse response = new GeneralResponse();

        try {
            CalendarUtil calendarUtil = new CalendarUtil();
            KajianTrx kajianTrx = new KajianTrx();
            kajianTrx.setKajianName(addKajianRq.getKajianName());
            kajianTrx.setSpeakerName(addKajianRq.getSpeakerName());
            kajianTrx.setSpeakerPictUrl(addKajianRq.getSpeakerPictUrl());
            kajianTrx.setLocation(addKajianRq.getLocation());
            kajianTrx.setTime(addKajianRq.getTime());
            kajianTrx.setLiveUrl(addKajianRq.getLiveUrl());
            kajianTrx.setQnaUrl(addKajianRq.getQnaUrl());
            kajianTrx.setDay(calendarUtil.getDayIdn(addKajianRq.getDay()));
            kajianTrx.setKajianDate(addKajianRq.getKajianDate());
            logger.info(String.valueOf(kajianTrx));
            sourceListKajian.insertKajianMaster(kajianTrx);
        } catch (Exception e){
            e.printStackTrace();
            logger.error(e.getMessage());
            response.setCode("500");
            response.setMessage("INTERNAL_SERVER_ERROR");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        response.setCode("200");
        response.setMessage("Request Success");
        return new ResponseEntity<>(response,HttpStatus.OK);
    }

    @GetMapping("/v1/kajian/show")
    public ResponseEntity<?> showListKajian(@RequestParam(value = "type") String type) throws Exception{
        logger.info("============ REQUEST GET LIST KAJIAN BY TYPE {} ============",type);
        List<ListKajianRs> list = new ArrayList<>();

        if (type.isEmpty()){
            GeneralResponse response = new GeneralResponse();
            response.setCode("400");
            response.setMessage("Please fill the type");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }

        switch (type){
            case "live":
                logger.info("live");
                list = sourceListKajian.getDataLive();
                break;
            case "week":
                logger.info("week");
                list = sourceListKajian.getDataWeek();
                break;
            case "archive":
                logger.info("archive");
                list = sourceListKajian.getDataArchive();
                break;
            default:
                logger.info("default");
                GeneralResponse response = new GeneralResponse();
                response.setCode("400");
                response.setMessage("Type not Found");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(list,HttpStatus.OK);
    }
}
